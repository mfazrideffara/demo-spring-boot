package com.example.demo.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Book;

//Digunakan untuk menandakan bahwa ini adalah repository
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	List<Book> findByStatusPeminjaman (int statusPeminjaman);
	List<Book> findByTitleBook(String titleBook);
}