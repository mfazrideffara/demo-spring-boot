package com.example.demo.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;

//untuk menandakan bahwa class ini merupakan controller
@RestController

//untuk slash buku ketika mulai digunakan
@RequestMapping("/Book")
public class BookController {

	@Autowired
	BookRepository bookRepository;
	
	
	@GetMapping("/")
	public List<Book> getAll(){
		return bookRepository.findAll();
	}
	
	@PostMapping("/")
	public Book tambahBook(@Valid @RequestBody Book book) {
		return bookRepository.save(book);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable(value="id")Long id,
	@Valid @RequestBody Book detailBook){
		Book book = bookRepository.findById(id)
				  .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		if(book == null) {
			return ResponseEntity.notFound().build();
		}
		book.setTitleBook(detailBook.getTitleBook());
		book.setNamaDepanPengarang(detailBook.getNamaDepanPengarang());
		book.setNamaBelakangPengarang(detailBook.getNamaBelakangPengarang());
		book.setNamaPeminjam(detailBook.getNamaPeminjam());
		book.setStatusPeminjaman(detailBook.getStatusPeminjaman());
		Book updatedBook = bookRepository.save(book);
		return ResponseEntity.ok(updatedBook);
	}
	
	@DeleteMapping("/{id}")
	public String deleteBook(@PathVariable (value="id") Long id){
		Book book = bookRepository.findById(id)
				  .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		String result = "";
		if(book == null) {
			result = "id "+id+" tidak ditemukan";
			return result;
		}
		result = "id "+id+" berhasil di hapus";
		bookRepository.delete(book);
		return result; 
	}
	
	@GetMapping("/{id}")
		public ResponseEntity<Book> getBookById(@PathVariable(value="id") Long id){
		Book book = bookRepository.findById(id)
				  .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		if(book == null) {
		return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(book);
	}
	
	@GetMapping("/sortBook")
	public List<Book> sortBook(@RequestParam(value="title")String titleBook){
		return bookRepository.findByTitleBook(titleBook);
	}
	
	@GetMapping("/sortstatus/{statusPeminjaman}")
	public List<Book> sortstatus(@PathVariable(value="statusPeminjaman") int statusPeminjaman){
		return bookRepository.findByStatusPeminjaman(statusPeminjaman);
	}
}
