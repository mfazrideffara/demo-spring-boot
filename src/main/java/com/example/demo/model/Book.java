package com.example.demo.model;

import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
//Digunakan untuk membuat table baru dengan nama “books”
@Table(name = "books")

//Merupakan entity dari JPA yang digunakan untuk meng-update data
@EntityListeners(AuditingEntityListener.class)

//Digunakan agar data pada variable tersebut tidak bisa dimasukan secara manual.
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)

public class Book implements Serializable{
	//Digunakan untuk memdefinisikan primary key
	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long id;
	
	//Digunakan agar isi pada field nya NOT NULL
	@NotBlank
	private String titleBook;
	
	@NotBlank
	private String namaDepanPengarang;
	
	@NotBlank
	private String namaBelakangPengarang;
	
	@NotBlank
	private int statusPeminjaman;
	
	@NotBlank
	private String namaPeminjam;
	
	//Digunakan untuk memberikan pengaturan tambahan pada kolom tersebut
	@Column(nullable = false, updatable = false)
	
	//Digunakan untuk merubah data berupa tanggal dan waktu dari database dan ke database
	@Temporal (TemporalType.TIMESTAMP)
	
	//Digunakan untuk mendapatkan waktu dibuatnya data
	@CreatedDate
	private Date createdAt;
	
	//Digunakan untuk memberikan pengaturan tambahan pada kolom tersebut
	@Column(nullable = false)
	
	//Digunakan untuk merubah data berupa tanggal dan waktu dari database dan ke database
	@Temporal(TemporalType.TIMESTAMP)
	
	//Digunakan untuk mendapatkan waktu terakhir data diedit
	@LastModifiedDate
	private Date updatedAt;
	
	//Method Setter Getter 
	public Long getId() {
		return id;
	}
	
	public String getTitleBook() {
		return titleBook;
	}
	
	public String getNamaDepanPengarang() {
		return namaDepanPengarang;
	}
	
	public String getNamaBelakangPengarang() {
		return namaBelakangPengarang;
	}
	
	public int getStatusPeminjaman() {
		return statusPeminjaman;
	}
	
	public String getNamaPeminjam() {
		return namaPeminjam;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setTitleBook(String titleBook) {
		this.titleBook = titleBook;
	}
	
	public void setNamaDepanPengarang(String namaDepanPengarang) {
		this.namaDepanPengarang = namaDepanPengarang;
	}
	
	public void setNamaBelakangPengarang(String namaBelakangPengarang) {
		this.namaBelakangPengarang = namaBelakangPengarang;
	}
	
	public void setStatusPeminjaman(int statusPeminjaman) {
		this.statusPeminjaman = statusPeminjaman;
	}
	
	public void setNamaPeminjam(String namaPeminjam) {
		this.namaPeminjam = namaPeminjam;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}